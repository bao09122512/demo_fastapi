import database as _datebase

def create_datebase():
    return _datebase.Base.metadata.create_all(bind=_datebase.engine)