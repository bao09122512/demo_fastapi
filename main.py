from email.policy import default
from typing import Union
from unittest import result
from unittest.mock import patch
from fastapi import Body, FastAPI, Path, Query
from enum import Enum
from pydantic import BaseModel, EmailStr, Field,Required

class ModelName(str, Enum):
    bao = 'Thien Bao'
    robin = 'martin robin'
    kevin = 'Jonst kevin'


class Items(BaseModel):
    name: str
    description: str | None = None
    price: float
    tax: float | None = None


app = FastAPI()

fake_items_db = [{"item_name": "Foo"}, {
    "item_name": "Bar"}, {"item_name": "Baz"}]

# @app.get("/items")
# async def root():
#     return {"message": "Hello World"}
# @app.get("/users/me")
# async def read_user_me():
#     return {"user_id": "the current user"}

# @app.get("/users/{user_id}")
# async def read_user(user_id: str):
#     return {"user_id": user_id}

# @app.get("/models/{model_name}")
# async def get_model(model_name: ModelName):
#     if model_name is ModelName.bao:
#         return {"model_name": model_name, "message": "day la bao"}
#     if model_name.value == "Jonst kevin":
#         return {"model_name": model_name, "message": "day la kevin"}
#     return {"model_name": model_name, "message": "day la robin"}


# @app.get("/files/{file_path:path}")
# async def read_file(file_path:str):
#     return {"file_path": file_path}

# @app.get("/items/")
# async def read_item(pagenum: int = 0, pagesize: int = 1):
#     return fake_items_db[pagenum : pagenum + pagesize]

# @app.get("/items/{item_id}")
# async def read_item(item_id: str, q:Union[str, None] = None):
#     if q:
#         return{"item_id": item_id, "q":q}
#     return {"item_id": item_id}

# @app.get("/itemsboolean/{item_id}")
# async def read_item(item_id: str, q: Union[str, None] = None, short: bool = False):
#     item = {"item_id": item_id}
#     if q:
#         item.update({"q": q})
#     if not short:
#         item.update(
#             {"description": "short false hien thi"}
#         )
#     return item

# @app.get("/items/{item_id}")
# async def read_user_item(
#     item_id: str, needy: str, skip: int = 0, limit: Union[int, None] = None
# ):
#     item = {"item_id": item_id, "needy": needy, "skip": skip, "limit": limit}
#     return item


# Request body

# @app.post("/items/")
# async def create_item(item: Items):
#     item_dict = item.dict()
#     if item.tax:
#         price_with_tax = item.price + item.tax
#         item_dict.update({"price_with_tax": price_with_tax})
#     return item_dict


# request body + path

# @app.put("/item/{item_id}")
# async def create_item(item_id: int, item:Items):
#     return {"item_id": item_id, **item.dict()}

# request body + path + query
# @app.put("/items/{item_id}")
# async def create_item(item_id:int, item: Items, q:str|None = None):
#     result = {"item_id": item_id, **item.dict()}
#     if q:
#         result.update({"q": q})
#     return result

# Query parameter

# @app.get("/items/")
# async def read_items(q:str |None = Query(default = "fixedquery",min_length=3, max_length= 50)):
#     results = {"items": [{"item_id": "Foo"}, {"item_id":"Bar"}]}
#     if q:
#         results.update({"q": q})
#     return results


# Required with Ellipsis (...) ràng buộc dấu 3 chấm
# @app.get("/units/")
# async def read_units(q: str = Query(default=Required, min_length=3)):
#     results = {"units": [{"unit_id": "Foo"}, {"unit_id": "Bar"}]}
#     if q:
#         results.update({"q": q})
#     return results

# Query parameter list / multiple values
# @app.get("/items/")
# async def read_items(q: list[str] = Query(default=["foo", "bar"])):
#     query_items = {"q": q}
#     return query_items

# @app.get("/items/")
# async def read_items(
#     q: str | None = Query(
#         default=None,
#         alias="item-query",
#         title="Query string",
#         min_length=3,
#         description="Query string for the item",
#         # deprecated=True,
#         )
# ):
#     results = {"items": [{"item_id": "Foo"}, {"item_id": "Bar"}]}
#     if q:
#         results.update({"q": q})
#     return results

# @app.get("/items/")
# async def read_item(
#     hidden_query: Union[str , None] = Query(default=None, include_in_schema=True)
# ):
#     if hidden_query:
#         return {"hidden_query": hidden_query}
#     else:
#         return {"hidden_query": "not found"}


################# Path params & numberic validations ##############################
# gt: greater than (lớn hơn)
# ge: greater than or equal (lớn hơn hoặc bằng)
# lt: less than (nhỏ hơn)
# le: less than or equal (nhỏ hơn hoặc bằng)
# @app.get("/items/{item_id}")
# async def read_items(
#     ## q là tham số không bắt buộc
#     # item_id: int = Path(title="The ID of the item to get"),
#     # q: str | None = Query(default=None, alias="item-query"),
#     ## muốn q khai báo required trong trường hợp này thì: chỉ cần truyền * làm tham số đầu tiên của hàm
#     ## trong python nó sẽ ngầm hiểu là các tham số sau * sẽ là key value pairs
#     *,
#     item_id: int = Path(title="The ID of the item to get", ge=1),# ge=1 : hiểu items_id là số nguyên >=1, gt= :lớn hơn, le= :nhỏ hơn hoặc bằng
#     q: str,
#     size: float = Query(gt=0, lt=10.5)
# ):
#     results = {"item_id": item_id}
#     if q:
#         results.update({"q": q})
#     return results


####### body-fields ########
# class Item(BaseModel):
#     name: str
#     description: str | None = Field(
#         default=None, title="The description of the item", max_length=300
#     )
#     price: float = Field(gt=0, description="The price must be greater than zero")
#     tax: float | None = None


# @app.put("/items/{item_id}")
# async def update_item(item_id: int, item: Item = Body(embed=True)):
#     results = {"item_id": item_id, "item": item}
#     return results

####### Body - Nested Models ########
# class Item(BaseModel):
#     name: str
#     description: str | None = None
#     price: float
#     tax: float | None = None
#     tags: list[str] = []


# @app.put("/items/{item_id}")
# async def update_item(item_id: int, item: Item):
#     results = {"item_id": item_id, "item": item}
#     return results


####### response model ######
# class Item(BaseModel):
#     name: str
#     description: str | None = None
#     price: float
#     tax: float | None = None
#     tags: list[str] = []


# @app.post("/items/", response_model=Item)
# async def create_item(item: Item):
#     return item

# class UserIn(BaseModel):
#     username: str
#     password: str
#     email: str
#     full_name: str | None = None
# class UserOut(BaseModel):
#     username: str
#     email: str
#     full_name: str | None = None

# # Don't do this in production!
# @app.post("/user/", response_model=UserOut)
# async def create_user(user: UserIn):
#     return user

#trả ra response với giá trị sẵn
# class Item(BaseModel):
#     name: str
#     description: str | None = None
#     price: float
#     tax: float = 10.5
#     tags: list[str] = []


# items = {
#     "foo": {"name": "Foo", "price": 50.2},
#     "bar": {"name": "Bar", "description": "The bartenders", "price": 62, "tax": 20.2},
#     "baz": {"name": "Baz", "description": None, "price": 50.2, "tax": 10.5, "tags": []},
# }


# @app.get("/items/{item_id}", response_model=Item, response_model_exclude_unset=True)
# async def read_item(item_id: str):
#     return items[item_id]

#
class Item(BaseModel):
    name: str
    description: str | None = None
    price: float
    tax: float = 10.5


items = {
    "foo": {"name": "Foo", "price": 50.2},
    "bar": {"name": "Bar", "description": "The Bar fighters", "price": 62, "tax": 20.2},
    "baz": {
        "name": "Baz",
        "description": "There goes my baz",
        "price": 50.2,
        "tax": 10.5,
    },
}


@app.get(
    "/items/{item_id}/name",
    response_model=Item,
    response_model_include={"name", "description"},
)
async def read_item_name(item_id: str):
    return items[item_id]


@app.get("/items/{item_id}/public", response_model=Item, response_model_exclude={"tax"})
async def read_item_public_data(item_id: str):
    return items[item_id]



####### extra model #######

class UserIn(BaseModel):
    username: str
    password: str
    email: str
    full_name: str | None = None


class UserOut(BaseModel):
    username: str
    email: str
    full_name: str | None = None


class UserInDB(BaseModel):
    username: str
    hashed_password: str
    email: str
    full_name: str | None = None


def fake_password_hasher(raw_password: str):
    return "supersecret" + raw_password


def fake_save_user(user_in: UserIn):
    hashed_password = fake_password_hasher(user_in.password)
    user_in_db = UserInDB(**user_in.dict(), hashed_password=hashed_password) #dict trả về giá trị cùng với mô hình
    print("User saved! ..not really")
    return user_in_db


@app.post("/user/", response_model=UserOut)
async def create_user(user_in: UserIn):
    user_saved = fake_save_user(user_in)
    return user_saved
